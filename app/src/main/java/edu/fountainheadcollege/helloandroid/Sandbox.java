package edu.fountainheadcollege.helloandroid;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by nicholson on 7/26/2016.
 */
public class Sandbox {

    private List<String> mGreetings = new ArrayList<String>();
    public static final String HELLO = "Hello Sandbox";

    //##########################################################
    //       CONSTRUCTOR
    //##########################################################
    public Sandbox() {

    }

    public List<String> getmGreetings() {
        return mGreetings;
    }

    public void setmGreetings(List<String> mGreetings) {
        this.mGreetings = mGreetings;
    }

    public boolean add(String object) {
        return mGreetings.add(object);
    }

    @Override
    public String toString() {
        return "Sandbox{" +
                "mGreetings=" + mGreetings +
                '}';
    }
}
